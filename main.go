package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/ssh"
	"net/http"
	"os"
	"strings"
	"time"
)

func main() {
	t := time.Now().Unix()

	var url, username, privateKey string

	flag.StringVar(&url, "url", "", "The base-url of the k8-proxy-backend")
	flag.StringVar(&username, "username", "", "The username of the user")
	flag.StringVar(&privateKey, "key", "", "Private key that should be used to login")

	flag.Parse()

	// Fix line breaks inside private key
	privateKey = strings.Replace(privateKey, "\\n", "\n", -1)

	if url == "" || username == "" || privateKey == "" {
		fmt.Fprintln(os.Stderr, "url, username and key arguments are required for authorization")
		os.Exit(1)
	}

	token := jwt.NewWithClaims(jwt.SigningMethodES256, jwt.MapClaims{
		"sub": username,
		"iat": t,
		"exp": t + 60*5,
	})

	priv, err := ssh.ParseRawPrivateKey([]byte(privateKey))
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	tokenString, err := token.SignedString(priv)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	if flag.Arg(0) != "update" || flag.Arg(1) != "container" {
		fmt.Fprintln(os.Stderr, "subcommand "+flag.Arg(0)+" "+flag.Arg(1)+"not found")
		os.Exit(1)
	}

	updateContainer(url, tokenString, os.Args[6:])
}

type updateRequest struct {
	Image     string `json:"image"`
	Container string `json:"container"`
}

func updateContainer(url string, token string, args []string) {
	var namespace, deployment, container, image string

	listCommand := flag.NewFlagSet("update", flag.ExitOnError)
	listCommand.StringVar(&namespace, "namespace", "", "Namespace inside which the deployment is")
	listCommand.StringVar(&deployment, "deployment", "", "Deployment inside which the container is")
	listCommand.StringVar(&container, "container", "", "Container that should be updated")
	listCommand.StringVar(&image, "image", "", "The new image that should be used")
	listCommand.Parse(args)

	if namespace == "" || deployment == "" || container == "" || image == "" {
		fmt.Fprintln(os.Stderr, "namespace, deployment, container and image arguments are required to update a container")
		os.Exit(1)
	}

	body := updateRequest{
		Image:     image,
		Container: container,
	}
	jsonBytes, err := json.Marshal(body)
	if err != nil {
		fmt.Fprintln(os.Stderr, "error while serializing request body", err)
		os.Exit(1)
	}

	client := &http.Client{}

	req, err := http.NewRequest("POST", url+"/namespaces/"+namespace+"/deployments/"+deployment+"/updates", bytes.NewBuffer(jsonBytes))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+token)
	if err != nil {
		fmt.Fprintln(os.Stderr, "error while crating request", err)
		os.Exit(1)
	}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Fprintln(os.Stderr, "error while requesting update", err)
		os.Exit(1)
	}

	if resp.StatusCode != 200 {
		fmt.Fprintln(os.Stderr, "error while update: ", resp.StatusCode)
		os.Exit(1)
	}
}
