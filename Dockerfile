FROM alpine:3.8

MAINTAINER Michael Braun
EXPOSE 8080/tcp

ENV ROOT_PATH /var/www

RUN apk add bash ca-certificates && \
    update-ca-certificates && \
    mkdir /var/www && \
    mkdir /home/go && \
    addgroup go && adduser -S -G go go && \
    chown -R go:go /var/www && \
    chown -R go:go /home/go

COPY ./k8-proxy /usr/local/bin/k8-proxy

RUN chown -R go:go /usr/local/bin/k8-proxy && \
    chmod +x /usr/local/bin/k8-proxy

USER go:go

CMD ["k8-proxy"]